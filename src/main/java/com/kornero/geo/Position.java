package com.kornero.geo;

import javax.annotation.concurrent.Immutable;

@Immutable
public class Position {

    private final String address;
    private final String lat;
    private final String lng;

    private Position() {
        this(null, null, null);
    }

    public Position(final String address, final String lat, final String lng) {
        this.address = address;
        this.lat = lat;
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;

        final Position position = (Position) o;

        if (address != null ? !address.equals(position.address) : position.address != null) return false;
        if (lat != null ? !lat.equals(position.lat) : position.lat != null) return false;
        return lng != null ? lng.equals(position.lng) : position.lng == null;

    }

    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + (lat != null ? lat.hashCode() : 0);
        result = 31 * result + (lng != null ? lng.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Position{" +
                "address='" + address + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                '}';
    }
}