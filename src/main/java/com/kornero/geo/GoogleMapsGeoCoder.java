package com.kornero.geo;

import com.kornero.http.SimpleHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.regex.Pattern;

public class GoogleMapsGeoCoder extends AbstractGeoCoder {

    private static final Logger logger = LoggerFactory.getLogger(GoogleMapsGeoCoder.class);

    private static final Pattern PATTERN_LAT = Pattern.compile("<lat>(\\d{2}\\.\\d+?)</lat>");
    private static final Pattern PATTERN_LNG = Pattern.compile("<lng>(\\d{2}\\.\\d+?)</lng>");
    private static final String OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT";

    private static final int TIMEOUT = 1000;
    public static final String API_URL = "http://maps.googleapis.com/maps/api/geocode/xml?region=ru&components=country:RU&address=";

    public GoogleMapsGeoCoder(final SimpleHttpClient simpleHttpClient) {
        super(TIMEOUT, simpleHttpClient);
    }

    @Nullable
    @Override
    protected Position parsePosition(final String address, final String response) {
        final String lat = getFirstGroupInPattern(PATTERN_LAT, response);
        final String lng = getFirstGroupInPattern(PATTERN_LNG, response);

        if (StringUtils.isBlank(lat) || StringUtils.isBlank(lng)) {
            return null;
        }

        return new Position(address, lat, lng);
    }

    @NotNull
    protected String getRequestUrl(final String address) {
//        return API_URL + encodeAddressForUrl("Россия, " + address);
        return API_URL + encodeAddressForUrl(address);
    }

    @Override
    protected boolean isTimeout(final String response) {
        return StringUtils.containsIgnoreCase(response, OVER_QUERY_LIMIT);
    }
}