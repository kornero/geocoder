package com.kornero.geo;

import com.kornero.http.SimpleHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.regex.Pattern;

public class YandexMapsGeoCoder extends AbstractGeoCoder {

    private static final Logger logger = LoggerFactory.getLogger(YandexMapsGeoCoder.class);

    private static final Pattern PATTERN_LAT = Pattern.compile(" (\\d{2}\\.\\d+?)</pos>");
    private static final Pattern PATTERN_LNG = Pattern.compile("<pos>(\\d{2}\\.\\d+?) ");

    private static final int TIMEOUT = 1000;
    public static final String API_URL = "http://geocode-maps.yandex.ru/1.x/?geocode=";

    public YandexMapsGeoCoder(final SimpleHttpClient simpleHttpClient) {
        super(TIMEOUT, simpleHttpClient);
    }

    @Nullable
    @Override
    protected Position parsePosition(final String address, final String response) {
        final String lat = getFirstGroupInPattern(PATTERN_LAT, response);
        final String lng = getFirstGroupInPattern(PATTERN_LNG, response);

        if (StringUtils.isBlank(lat) || StringUtils.isBlank(lng)) {
            return null;
        }

        return new Position(address, lat, lng);
    }

    @Nonnull
    @NotNull
    @Override
    protected String getRequestUrl(final String address) {
        return API_URL + encodeAddressForUrl(address);
    }

    @Override
    protected boolean isTimeout(final String response) {
        return false;
    }
}