package com.kornero.geo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * TODO: Add new services:
 * http://demo.api.2gis.ru/geoSearch?q=%D0%A5%D0%90%D0%91%D0%90%D0%A0%D0%9E%D0%92%D0%A1%D0%9A%D0%98%D0%99+%D0%9A%D0%A0%D0%90%D0%99%2C+%D0%93.%D0%A5%D0%90%D0%91%D0%90%D0%A0%D0%9E%D0%92%D0%A1%D0%9A%2C+%D0%A3%D0%9B.%D0%92%D0%9E%D0%A0%D0%9E%D0%9D%D0%95%D0%96%D0%A1%D0%9A%D0%90%D0%AF%2C+%D0%94.49%2C+%D0%9A%D0%9E%D0%A0%D0%9F%D0%A3%D0%A11&searchType=byName
 * http://ru.mygeoposition.com/
 * https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=%D0%A5%D0%90%D0%91%D0%90%D0%A0%D0%9E%D0%92%D0%A1%D0%9A%D0%98%D0%99%20%D0%9A%D0%A0%D0%90%D0%99%2C%20%D0%93.%D0%A5%D0%90%D0%91%D0%90%D0%A0%D0%9E%D0%92%D0%A1%D0%9A%2C%20%D0%A3%D0%9B.%D0%92%D0%9E%D0%A0%D0%9E%D0%9D%D0%95%D0%96%D0%A1%D0%9A%D0%90%D0%AF%2C%20%D0%94.49%2C%20%D0%9A%D0%9E%D0%A0%D0%9F%D0%A3%D0%A11&f=json&outSR=%7B%22wkid%22%3A102100%7D&outFields=Match_addr%2CAddr_type%2CStAddr%2CCity&distance=50000&location=%7B%22x%22%3A15035571.851856556%2C%22y%22%3A6192105.237724395%2C%22spatialReference%22%3A%7B%22wkid%22%3A102100%7D%7D&maxLocations=5
 */
public interface GeoCoder {

    @Nullable
    Position getPosition(@Nonnull final String address);
}