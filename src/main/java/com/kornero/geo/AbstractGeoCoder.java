package com.kornero.geo;

import com.kornero.http.SimpleHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractGeoCoder implements GeoCoder {

    private static final Logger logger = LoggerFactory.getLogger(AbstractGeoCoder.class);

    private final int timeout;
    private final SimpleHttpClient simpleHttpClient;

    public AbstractGeoCoder(final int timeout, final SimpleHttpClient simpleHttpClient) {
        this.timeout = timeout;
        this.simpleHttpClient = simpleHttpClient;
    }

    @Nullable
    @Override
    public Position getPosition(@NotNull final String address) {
        final String response;
        try {
            response = makeRequest(address);
        } catch (final InterruptedException e) {
            logger.error("getPosition(): ", e);
            return null;
        }

        return parsePosition(address, response);
    }

    @Nullable
    protected abstract Position parsePosition(final String address, final String response);

    @NotNull
    @Nonnull
    protected abstract String getRequestUrl(final String address);

    protected abstract boolean isTimeout(final String response);

    protected String makeRequest(final String address) throws InterruptedException {
        final String urlToRead = getRequestUrl(address);
        final String response;
        try {
            response = simpleHttpClient.getHtmlViaProxy(urlToRead);
        } catch (final IOException e) {
            logger.error("makeRequest(): ", e);
            Thread.sleep(timeout);
            return makeRequest(address);
        }

        if (isTimeout(response)) {
            logger.warn("makeRequest(): server is down. Url = '{}'", urlToRead);
            Thread.sleep(timeout);
            return makeRequest(address);
        }

        return response;
    }

    @Nonnull
    protected static String getFirstGroupInPattern(final Pattern pattern, final String xml) {
        final Matcher matcher = pattern.matcher(xml);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return StringUtils.EMPTY;
        }
    }

    @Nonnull
    protected static String encodeAddressForUrl(final String address) {
        try {
            return URLEncoder.encode(address, "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            logger.error("encodeAddressForUrl(): ", e);
            throw new RuntimeException(e);
        }
    }
}