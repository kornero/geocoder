package com.kornero.geo;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Tests {

    private static final Logger logger = LoggerFactory.getLogger(Tests.class);

    private static final GeoCoder GEO_CODER = GeoCoderFactory.getGoogleMapsGeoCoder();

    @Ignore
    @Test
    public void loadAndSaveDataForMoscow() throws IOException, InterruptedException {
        final String address = "Moscow, Ivana Franko 10"; // 55.727716, 37.448763
        final Position expected = new Position(address, "55.7278430", "37.4488060");

        final Position position = GEO_CODER.getPosition(address);

        Assert.assertNotNull("Nothing founded.", position);

        logger.info("Found {}", position);
        Assert.assertEquals("Wrong position.", expected, position);
    }
}